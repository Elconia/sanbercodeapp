import React from 'react';
import AppNavigation from './src/navigation/navigation';
import firebase from '@react-native-firebase/app';

var firebaseConfig = {
  apiKey: 'AIzaSyDEPcAVZCnI8XLsAvJaPvKQNzlqYNz7STs',
  authDomain: 'sanbercode-9aa13.firebaseapp.com',
  databaseURL: 'https://sanbercode-9aa13.firebaseio.com',
  projectId: 'sanbercode-9aa13',
  storageBucket: 'sanbercode-9aa13.appspot.com',
  messagingSenderId: '122705107667',
  appId: '1:122705107667:web:c65cfa7632e7e2f4a99226',
  measurementId: 'G-CR3X81LJK5',
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  return <AppNavigation />;
};

export default App;
