import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Axios from 'axios';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../api';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed!',
  cancelText: 'cancel',
};

function Login({navigation}) {
  const [email, setEmail] = useState('apotek.setia.jaya@gmail.com');
  const [password, setPassword] = useState('Qwerty1234');

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    navigation.dispatch((state) => {
      // Remove the intro route from the stack
      const routes = state.routes.filter((r) => r.name !== 'Intro');

      return CommonActions.reset({
        ...state,
        routes,
        index: routes.length - 1,
      });
    });

    configureGoogleSignIn();
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '122705107667-bal7abdim7ij6brockj5cro9ok6b784u.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      const credential = auth.GoogleAuthProvider.credential(idToken);

      auth().signInWithCredential(credential);
    } catch (error) {
      alert('Login Failed');
      console.log(error);
    }
  };

  const onLoginPress = () => {
    // let data = {
    //   email: email,
    //   password: password,
    // };
    // Axios.post(`${api}/login`, data, {timeout: 20000})
    //   .then((res) => {
    //     saveToken(res.data.token);
    //     navigation.navigate('Profile');
    //   })
    //   .catch((err) => {
    //     alert('Login Failed');
    //     console.log('Login -> err', err);
    //   });
    // alert("Please sign in using google!");

    return auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => false)
      .catch((err) => console.log(err));
  };

  // const signInWithFingerprint = () => {
  //   TouchID.authenticate('', config)
  //     .then((success) => {
  //       navigation.navigate('Dashboard');
  //     })
  //     .catch((error) => {
  //       alert('Authentication Failed!');
  //     });
  // };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
      <View style={styles.logoContainer}>
        <Image source={require('../assets/images/logo.jpg')} />
      </View>
      <Text style={styles.label}>Username</Text>
      <TextInput
        value={email}
        onChangeText={(email) => setEmail(email)}
        style={styles.textInput}
        underlineColorAndroid="#C6C6C6"
        placeholder="Username or Email"
      />
      <Text style={styles.label}>Password</Text>
      <TextInput
        secureTextEntry
        value={password}
        onChangeText={(password) => setPassword(password)}
        style={styles.textInput}
        underlineColorAndroid="#C6C6C6"
        placeholder="Password"
      />

      <TouchableOpacity style={styles.btnLogin} onPress={() => onLoginPress()}>
        <Text style={styles.loginTxt}>LOGIN</Text>
      </TouchableOpacity>

      <View style={styles.btnGoogle}>
        <GoogleSigninButton
          style={{width: '100%', height: 48}}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
          onPress={() => signInWithGoogle()}
        />
      </View>

      <TouchableOpacity
        style={styles.btnFingerprint}
        onPress={() => signInWithFingerprint()}>
        <Text style={styles.fingerprintTxt}>SIGN IN WITH FINGERPRINT</Text>
      </TouchableOpacity>

      <View style={styles.bottomContainer}>
        <Text>Belum mempunyai akun ? </Text>
        <TouchableOpacity>
          <Text style={{color: 'blue'}}>Buat Akun</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  logoContainer: {
    height: 200,
    margin: 48,
    alignItems: 'center',
    justifyContent: 'center',
  },

  label: {
    marginHorizontal: 24,
    fontWeight: 'bold',
  },

  textInput: {
    height: 40,
    marginHorizontal: 24,
    marginBottom: 12,
  },

  btnLogin: {
    height: 48,
    marginHorizontal: 24,
    marginTop: 12,
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
    justifyContent: 'center',
  },

  loginTxt: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    letterSpacing: 1,
  },

  btnGoogle: {
    marginHorizontal: 24,
    marginTop: 12,
  },

  btnFingerprint: {
    height: 48,
    marginHorizontal: 24,
    marginTop: 12,
    backgroundColor: '#191970',
    alignItems: 'center',
    justifyContent: 'center',
  },

  fingerprintTxt: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    letterSpacing: 1,
  },

  bottomContainer: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 24,
    marginBottom: 24,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});

export default Login;
