import React, {useEffect, useState} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

const ChatScreen = ({route, navigation}) => {
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState({});

  useEffect(() => {
    const user = auth().currentUser;
    setUser(user);
    getData();

    return () => {
      const db = database().ref("messages");
      if(db) {
        db.off();
      }
    }
  }, []);

  const getData = () => {
    database().ref('messages').limitToLast(20).on('child_added', snapshot => {
      const value = snapshot.val();
      setMessages(previousMessages => GiftedChat.append(previousMessages, value));
    })
  }
  const onSend = (messsages = []) => {
    for (let index = 0; index < messsages.length; index++) {
      database().ref('messages').push({
        _id: messsages[index]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messsages[index].text,
        user: messsages[index].user,
      });
    }
  };

  return (
    <GiftedChat
      messages={messages}
      onSend={(messsages) => onSend(messsages)}
      user={{
        _id: user.uid,
        name: user.email,
        avatar: '',
      }}
    />
  );
};

export default ChatScreen;
