import React from 'react';
import {
  StatusBar,
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const HomeScreen = ({navigation}) => {
  const summaryData = [
    {
      title: 'React Native',
      today: '20',
      total: '100',
    },
    {
      title: 'Data Science',
      today: '30',
      total: '100',
    },
    {
      title: 'React JS',
      today: '66',
      total: '100',
    },
    {
      title: 'Laravel',
      today: '60',
      total: '100',
    },
    {
      title: 'Wordpress',
      today: '45',
      total: '100',
    },
    {
      title: 'Desain Grafis',
      today: '35',
      total: '100',
    },
    {
      title: 'Web Server',
      today: '11',
      total: '100',
    },
    {
      title: 'UI/UX Design',
      today: '80',
      total: '100',
    },
  ];

  const FlatListItem = ({title, today, total}) => (
    <View style={styles.itemContainer}>
      <View style={styles.itemHeader}>
        <Text style={{color: 'white', fontWeight: 'bold'}}>{title}</Text>
      </View>
      <View style={styles.itemBody}>
        <Text style={{color: 'white'}}>Today</Text>
        <Text style={{color: 'white'}}>{today} orang</Text>
      </View>
      <View style={styles.itemBody}>
        <Text style={{color: 'white'}}>Total</Text>
        <Text style={{color: 'white'}}>{total} orang</Text>
      </View>
    </View>
  );

  const goToReactNativeScreen = () => {
    navigation.navigate('ReactNative');
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#3ec6ff" />
      {/* Top Kelas */}
      <View style={styles.classContainer}>
        <View style={styles.header}>
          <Text style={{color: 'white', fontSize: 16}}>Kelas</Text>
        </View>
        <View style={styles.body}>
          <TouchableOpacity style={styles.iconContainer} onPress={() => goToReactNativeScreen()}>
            <Icon
              style={styles.icon}
              name="logo-react"
              size={50}
              color="white"
            />
            <Text style={styles.iconText}>React Native</Text>
          </TouchableOpacity>
          <View style={styles.iconContainer}>
            <Icon
              style={styles.icon}
              name="logo-python"
              size={50}
              color="white"
            />
            <Text style={styles.iconText}>Data Science</Text>
          </View>
          <View style={styles.iconContainer}>
            <Icon
              style={styles.icon}
              name="logo-react"
              size={50}
              color="white"
            />
            <Text style={styles.iconText}>React JS</Text>
          </View>
          <View style={styles.iconContainer}>
            <Icon
              style={styles.icon}
              name="logo-laravel"
              size={50}
              color="white"
            />
            <Text style={styles.iconText}>Laravel</Text>
          </View>
        </View>
      </View>

      {/* Middle Kelas */}
      <View style={styles.classContainer}>
        <View style={styles.header}>
          <Text style={{color: 'white', fontSize: 16}}>Kelas</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.iconContainer}>
            <Icon
              style={styles.icon}
              name="logo-wordpress"
              size={50}
              color="white"
            />
            <Text style={styles.iconText}>Wordpress</Text>
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={styles.icon}
              source={require('../assets/icons/website-design.png')}
            />
            <Text style={styles.iconText}>Design Grafis</Text>
          </View>
          <View style={styles.iconContainer}>
            <MCIcon style={styles.icon} name="server" size={50} color="white" />
            <Text style={styles.iconText}>Web Server</Text>
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={styles.icon}
              source={require('../assets/icons/ux.png')}
            />
            <Text style={styles.iconText}>UI/UX Design</Text>
          </View>
        </View>
      </View>

      {/* Summary */}
      <View style={styles.summaryContainer}>
        <View style={styles.header}>
          <Text style={{color: 'white', fontSize: 16}}>Summary</Text>
        </View>

        <FlatList
          nestedScrollEnabled
          style={{flex: 1}}
          data={summaryData}
          keyExtractor={(item) => item.title}
          renderItem={({item}) => (
            <FlatListItem
              title={item.title}
              today={item.today}
              total={item.total}
            />
          )}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  classContainer: {
    margin: 16,
    marginBottom: 0,
    height: 140,
    borderRadius: 8,
    backgroundColor: '#3EC6FF',
  },
  summaryContainer: {
    flex: 1,
    margin: 16,
    marginBottom: 0,
    borderRadius: 8,
    backgroundColor: '#3EC6FF',
  },
  header: {
    height: 30,
    paddingLeft: 12,
    justifyContent: 'center',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: '#088dc4',
  },
  body: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  iconContainer: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 50,
    height: 50,
  },
  iconText: {
    color: 'white',
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  itemContainer: {
    backgroundColor: '#088dc4',
  },
  itemHeader: {
    height: 30,
    paddingLeft: 12,
    justifyContent: 'center',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: '#3EC6FF',
  },
  itemBody: {
    height: 30,
    paddingHorizontal: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default HomeScreen;
