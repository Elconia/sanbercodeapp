import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  StatusBar,
  TouchableOpacity,
  Modal,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

const Register = ({navigation}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);

  const toggleCamera = () => {
    setType(type == 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const option = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.takePictureAsync(option);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert('Upload Success');
      })
      .catch((error) => {
        alert(error);
      });
  };

  const RenderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            ref={(ref) => {
              camera = ref;
            }}
            type={type}
            captureAudio={false}>
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity
                style={styles.btnFlip}
                onPress={() => toggleCamera()}>
                <MaterialCommunity name="rotate-3d-variant" size={20} />
              </TouchableOpacity>
            </View>
            <View style={styles.round} />
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity
                style={styles.btnTake}
                onPress={() => takePicture()}>
                <Icon name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#3ec6ff" />
      <View style={styles.blueBackground} />
      <Image
        source={
          photo === null
            ? require('../assets/images/profile.png')
            : {uri: photo.uri}
        }
        style={styles.profileImage}
        resizeMode="cover"
      />
      <TouchableOpacity onPress={() => setIsVisible(true)}>
        <Text style={styles.profileName}>Change Picture</Text>
      </TouchableOpacity>
      <RenderCamera />

      <View style={styles.formContainer}>
        <View style={styles.row}>
          <Text style={styles.label}>Nama</Text>
          <TextInput placeholder="Nama" underlineColorAndroid="#c6c6c6" />
        </View>

        <View style={styles.row}>
          <Text style={styles.label}>Email</Text>
          <TextInput placeholder="Email" underlineColorAndroid="#c6c6c6" />
        </View>

        <View style={styles.row}>
          <Text style={styles.label}>Password</Text>
          <TextInput placeholder="Password" underlineColorAndroid="#c6c6c6" />
        </View>

        <TouchableOpacity style={styles.btnRegister} onPress={() => uploadImage(photo.uri)}>
          <Text style={styles.registerTxt}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  blueBackground: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '35%',
    backgroundColor: '#3ec6ff',
  },

  profileImage: {
    marginTop: '5%',
    width: '25%',
    height: '20%',
    borderRadius: 50,
  },

  profileName: {
    marginBottom: 24,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },

  formContainer: {
    width: '90%',
    height: '45%',
    padding: 24,
    borderRadius: 8,
    backgroundColor: 'white',

    //shadow
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },

  btnRegister: {
    height: 48,
    marginTop: 12,
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
    justifyContent: 'center',
  },

  registerTxt: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    letterSpacing: 1,
  },

  btnFlipContainer: {
    marginTop: 25,
    marginLeft: 20,
  },

  btnFlip: {
    backgroundColor: 'white',
    width: 50,
    height: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },

  round: {
    width: 180,
    height: 280,
    borderRadius: 90,
    borderWidth: 1,
    borderColor: 'white',
    alignSelf: 'center',
  },

  rectangle: {
    width: 200,
    height: 150,
    marginTop: 50,
    borderWidth: 1,
    borderColor: 'white',
    alignSelf: 'center',
  },

  btnTakeContainer: {
    marginTop: 30,
    alignItems: 'center',
  },

  btnTake: {
    backgroundColor: 'white',
    width: 80,
    height: 80,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Register;
