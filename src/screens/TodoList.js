import React, {useContext} from 'react';
import {
  StyleSheet,
  StatusBar,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList
} from 'react-native';
import {RootContext} from '../navigation/RootContext';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const TodoList = () => {
  const state = useContext(RootContext);

  const FlatListItem = ({id, date, value}) => {
    return (
      <View style={styles.itemContainer}>
        <View style={{flex: 1}}>
          <Text style={styles.pItem}>{date}</Text>
          <Text style={styles.pItem}>{value}</Text>
        </View>
        <TouchableOpacity
          style={styles.iconTrash}
          onPress={() => state.deleteItemById(id)}>
          <Icon name="trash-can-outline" size={25} />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <Text style={styles.label}>Masukkan Todolist</Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={{flex: 1, borderWidth: 2}}
          value={state.todo}
          placeholder="Input here"
          onChangeText={(input) => state.handleInput(input)}
        />
        <TouchableOpacity
          style={styles.buttonAdd}
          onPress={() => state.addItem()}>
          <Text style={{fontSize: 28}}>+</Text>
        </TouchableOpacity>
      </View>
      <FlatList
        style={{flex: 1}}
        data={state.data}
        keyExtractor={(item) => String(item.id)}
        renderItem={({item}) => (
          <FlatListItem id={item.id} date={item.tanggal} value={item.value} />
        )}
        ItemSeparatorComponent={() => <View style={{height: 12}} />}
        keyboardShouldPersistTaps="always"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: 'white',
  },

  label: {
    width: '100%',
    marginBottom: 8,
  },

  inputContainer: {
    width: '100%',
    marginBottom: 24,
    flexDirection: 'row',
  },

  buttonAdd: {
    width: 55,
    height: 55,
    marginLeft: 4,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3ec6ff',
  },

  itemContainer: {
    width: '100%',
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderRadius: 4,
    borderWidth: 5,
    borderColor: '#bdc3c7',
  },

  iconTrash: {
    width: 25,
    height: 25,
  },
});

export default TodoList;
