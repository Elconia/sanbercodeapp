import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  StatusBar,
  View,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
// import Axios from 'axios';
// import AsyncStorage from '@react-native-community/async-storage';
// import api from '../api';
import {GoogleSignin} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';

const Biodata = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);

  useEffect(() => {
    // const getToken = async () => {
    //   try {
    //     const token = await AsyncStorage.getItem('token');
    //     //return getVenue(token);
    //   } catch (e) {
    //     console.log(e);
    //   }
    // };

    // getToken();
    getCurrentUser();
    GoogleSignin.configure({});
  }, [userInfo]);

  const getCurrentUser = async () => {
    const user = auth().currentUser;
    if (user != null) {
      setUserInfo(user);
    }
    // try {
    //   const newUserInfo = await GoogleSignin.signInSilently();
    //   setUserInfo(newUserInfo);
    // } catch (e) {
    //   console.log(e);
    // }
  };

  // const getVenue = (token) => {
  //   Axios.get(`${api}/venues`, {
  //     timeout: 20000,
  //     headers: {
  //       Authorization: 'Bearer' + token,
  //     },
  //   })
  //     .then((res) => {
  //       console.log(res);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };

  const onLogoutPress = async () => {
    try {
      await GoogleSignin.revokeAccess();
      // await GoogleSignin.signOut();
      await auth().signOut();
      // await AsyncStorage.removeItem('token');
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#3ec6ff" />
      <View style={styles.blueBackground} />
      <Image
        source={{uri: userInfo && userInfo.photoURL}}
        style={styles.profileImage}
        resizeMode="contain"
      />
      <Text style={styles.profileName}>
        {userInfo && userInfo.displayName}
      </Text>
      <View style={styles.bioContainer}>
        <View style={styles.pRow}>
          <Text style={styles.pBio}>Tanggal Lahir</Text>
          <Text style={styles.pBio}>25 Juli 1991</Text>
        </View>
        <View style={styles.pRow}>
          <Text style={styles.pBio}>Jenis Kelamin</Text>
          <Text style={styles.pBio}>Laki - laki</Text>
        </View>
        <View style={styles.pRow}>
          <Text style={styles.pBio}>Hobi</Text>
          <Text style={styles.pBio}>Ngoding</Text>
        </View>
        <View style={styles.pRow}>
          <Text style={styles.pBio}>No. Telp</Text>
          <Text style={styles.pBio}>082140081682</Text>
        </View>
        <View style={styles.pRow}>
          <Text style={styles.pBio}>Email</Text>
          <Text style={styles.pBio}>
            {userInfo && userInfo.email}
          </Text>
        </View>
        <TouchableOpacity
          style={styles.btnLogout}
          onPress={() => onLogoutPress()}>
          <Text style={styles.logoutTxt}>LOGOUT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  blueBackground: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '35%',
    backgroundColor: '#3ec6ff',
  },

  profileImage: {
    marginTop: '5%',
    width: '25%',
    height: '20%',
    borderRadius: 50,
  },

  profileName: {
    marginBottom: 24,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },

  bioContainer: {
    width: '90%',
    height: '50%',
    padding: 24,
    borderRadius: 8,
    backgroundColor: 'white',

    //shadow
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },

  pRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  pBio: {
    marginBottom: 24,
  },

  btnLogout: {
    height: 48,
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
    justifyContent: 'center',
  },

  logoutTxt: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    letterSpacing: 1,
  },
});

export default Biodata;
