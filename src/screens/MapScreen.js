import React, {useEffect} from 'react';
import {View, StatusBar, StyleSheet} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoiZWxjb25pYSIsImEiOiJja2RmYXdlaXgxdDF5MnF0MTd2dTNraDJzIn0.vicl-ZxHzPXF_5doZVJDnw',
);

const coordinates = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.89869],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.60918, -6.898013],
];

const MapScreen = () => {
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log(error);
      }
    };

    getLocation();
  }, []);

  const MapBoxAnnotation = coordinates.map((value, index) => (
    <MapboxGL.PointAnnotation id={String(index)} key={index} coordinate={value}>
      <MapboxGL.Callout
        title={`Longitude: ${value[0]} Latitude: ${value[1]}`}
      />
    </MapboxGL.PointAnnotation>
  ));

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#3ec6ff" />
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {MapBoxAnnotation}
      </MapboxGL.MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});

export default MapScreen;
