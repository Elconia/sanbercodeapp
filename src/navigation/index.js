import React, {useState} from 'react';
import {Keyboard} from 'react-native';
import TodoList from '../screens/TodoList';
import {RootContext} from './RootContext';

const Context = () => {
  const [todo, setTodo] = useState('');
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);

  const handleInput = (input) => {
    setTodo(input);
  };

  const addItem = () => {
    if (todo) {
      const today = new Date();
      const day = today.getDate();
      const month = today.getMonth() + 1;
      const year = today.getFullYear();
      const date = `${day}/${month}/${year}`;
      const newData = {id: count, tanggal: date, value: todo};
      setData([...data, newData]);
      setTodo('');
      setCount(count + 1);
    }
  };

  const deleteItemById = (id) => {
    Keyboard.dismiss();
    const filteredData = data.filter((item) => item.id !== id);
    setData(filteredData);
  };

  return (
    <RootContext.Provider
      value={{todo, data, handleInput, addItem, deleteItemById}}>
      <TodoList />
    </RootContext.Provider>
  );
};

export default Context;
