import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SplashScreen from '../screens/Splashscreen';
import Intro from '../screens/Intro';
import Login from '../screens/Login';
import Register from '../screens/Register';
import Profile from '../screens/Biodata';
import HomeScreen from '../screens/HomeScreen';
import MapScreen from '../screens/MapScreen';
import ReactNativeScreen from '../screens/ReactNativeScreen';
import ChatScreen from '../screens/ChatScreen';

const Stack = createStackNavigator();
const HomeStack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HomeNavigation = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen
      name="Dashboard"
      component={DashboardNavigation}
      options={{headerShown: false}}
    />
    <HomeStack.Screen name="ReactNative" component={ReactNativeScreen} />
  </HomeStack.Navigator>
);

const DashboardNavigation = () => (
  <Tab.Navigator
    screenOptions={({route}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = focused ? 'home' : 'home-outline';
        } else if (route.name === 'Map') {
          iconName = focused
            ? 'map-marker-radius'
            : 'map-marker-radius-outline';
        } else if (route.name === 'Chat') {
          iconName = focused ? 'chat' : 'chat-outline';
        } else if (route.name === 'Profile') {
          iconName = focused ? 'account' : 'account-outline';
        }

        // You can return any component that you like here!
        return <Icon name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#3EC6FF',
      inactiveTintColor: 'gray',
    }}>
    <Tab.Screen name="Home" component={HomeScreen} />
    <Tab.Screen name="Map" component={MapScreen} />
    <Tab.Screen name="Chat" component={ChatScreen} />
    <Tab.Screen name="Profile" component={Profile} />
  </Tab.Navigator>
);

function AppNavigation() {
  // Detect the first and initial launch of an react-native app
  const [firstLaunch, setFirstLaunch] = useState(null);

  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    AsyncStorage.getItem('alreadyLaunched').then((value) => {
      if (value == null) {
        // No need to wait for `setItem` to finish, although you might want to handle errors
        AsyncStorage.setItem('alreadyLaunched', 'true');
        setFirstLaunch(true);
      } else {
        setFirstLaunch(false);
      }
    });

    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing || firstLaunch === null) return <SplashScreen />;

  /* mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []); */

  const MainNavigation = () =>
    firstLaunch ? (
      <Stack.Navigator>
        <Stack.Screen
          name="Intro"
          component={Intro}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    ) : (
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );

  return (
    <NavigationContainer>
      {!user ? <MainNavigation /> : <HomeNavigation />}
    </NavigationContainer>
  );
}

export default AppNavigation;
